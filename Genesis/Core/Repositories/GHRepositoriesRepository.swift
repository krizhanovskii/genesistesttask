import Foundation
import Alamofire
import RxSwift
import RxCocoa
import RealmSwift

class GHRepositoriesRepository {
    // apiclient
    static fileprivate let apiClient = RestAPIClient()
    static private let bag = DisposeBag()
    
    
    static func searchRepositories(_ parameters: DictionaryAlias, returnRequest: ((Request?)->Void)? = nil) -> Observable<GHRepositorySearchResponse> {
        let request = GHRepositorySearchRequest()
        request.parameters = parameters
        return apiClient.rx_execute(encoding: URLEncoding.default, request: request, returnRequest:returnRequest)
    }
    
    
    
    static func fetchHistory<T:Object>(_ type:T.Type) -> Observable<Array<T>> {
        return Observable.create { observer in
            
            RealmResolver.fetchAllItems(T.self, success: { (items) in
                observer.on(.next(items))
                observer.on(.completed)
            }) { (error) in
                observer.onError(error)
            }
            return Disposables.create {
            }
        }
        
    }
}
