//
//  UINavigationExtension.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import Foundation
import UIKit


import Foundation
import UIKit


enum NavigationStyle {
    case `default`
    case tranparent
    case imaged
}


class RootNavigationCntrl: UINavigationController {
    var shadowView : UIView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarStyle(.default)
    }
}

extension UINavigationController {
    
    func setNavigationBarStyle(_ style: NavigationStyle) {
        switch style {
        case .tranparent:
            self.setTransparentNavigationBarStyle()
        case .imaged:
            self.setImagedNavigationBarStyle()
            
        default:
            self.setDefaultNavigationBarStyle()
        }
    }
    
    
    fileprivate func setTransparentNavigationBarStyle() {
        self.navigationBar.barTintColor = .clear
        self.navigationBar.isTranslucent = true
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.setSeparatorShowed(false)
        self.childViewControllers.first?.automaticallyAdjustsScrollViewInsets = false
    }
    
    
    fileprivate func setDefaultNavigationBarStyle() {
        self.navigationBar.barTintColor = .black
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.setSeparatorShowed(true)
        
    }
    
    fileprivate func setImagedNavigationBarStyle() {
    }
    
    
    func setSeparatorShowed(_ flag : Bool) {
        if flag == true {
            self.navigationBar.shadowImage = nil
        } else {
            self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationBar.shadowImage = UIImage()
        }
    }
    
    
    
    
    public func pushViewController(viewController: UIViewController,
                                   animated: Bool,
                                   completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
}
