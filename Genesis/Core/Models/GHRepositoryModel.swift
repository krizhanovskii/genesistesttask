import Foundation
import RealmSwift

class GHRepositoryModel: Object, ModelProtocol, Listable {
    
    dynamic var id: Int = intDummy
    dynamic var webUrl: String = stringDummy
    dynamic var name: String = stringDummy
    dynamic var fullName: String = stringDummy
    dynamic var repositoryDescription: String = stringDummy
    dynamic var language: String = stringDummy
    dynamic var starCount: Int = intDummy
    
    override static func primaryKey() -> String? {
        return "id"
    }

    required convenience init(resp: DictionaryAlias) {
        self.init()
        self.id = resp["id"] as? Int ?? intDummy
        self.webUrl = resp["html_url"] as? String ?? stringDummy
        self.name = resp["name"] as? String ?? stringDummy
        self.fullName = resp["full_name"] as? String ?? stringDummy
        self.repositoryDescription = resp["description"] as? String ?? stringDummy
        self.language = resp["language"] as? String ?? stringDummy
        self.starCount = resp["stargazers_count"] as? Int ?? intDummy
    }
    
}
