import Foundation


import RealmSwift

class GHRepositoryHistoryModel: Object, ModelProtocol, Listable {
    
    dynamic var time: Int = 0
    dynamic var searchRequest: String = stringDummy
    let results: List<GHRepositoryModel> = List<GHRepositoryModel>()
    
    override static func primaryKey() -> String? {
        return "time"
    }
    
    required convenience init(resp: DictionaryAlias) {
        self.init()
    }
    
}
