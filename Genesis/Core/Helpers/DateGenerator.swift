import Foundation

enum DateStyleGenerator: String {

    case timeURC = "yyyy-MM-dd'T'HH:mm:ss+ssss"
    case dateAndTime = "dd/MM/yyyy, HH:mm"
    
    fileprivate func getDateFormatter(_ timeZone:TimeZone) -> DateFormatter {
        let dateFromatter = DateFormatter()
        dateFromatter.timeZone = timeZone
        return dateFromatter
    }
    
    func stringFromDate(_ date: Date, locale: Locale = Locale.current, timeZone:TimeZone = .current) -> String {
        let dateFromatter = self.getDateFormatter(timeZone)
        dateFromatter.dateFormat = self.rawValue
        dateFromatter.locale = locale
        return dateFromatter.string(from: date)
    }
    
    func dateFromString(_ string:String, locale: Locale = Locale.current, timeZone:TimeZone = .current) -> Date? {
        let dateFromatter = self.getDateFormatter(timeZone)
        dateFromatter.locale = locale
        dateFromatter.dateFormat = self.rawValue
        return dateFromatter.date(from: string)
    }
    
}


extension Date {
    
}
