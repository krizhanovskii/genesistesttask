import Foundation
import CoreData
import RealmSwift


class DataBaseConfiguration {
    //SET new value
    static let dbVersion: UInt64 = 1
    
    static func configure() {
        var config = Realm.Configuration (
            schemaVersion: dbVersion,
            
            migrationBlock: { migration, oldSchemaVersion in
                            },
            deleteRealmIfMigrationNeeded:true
        )
        
        
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("genesisVersion\(dbVersion).realm")
                Log.info.log("REALM URL: \(config.fileURL?.absoluteString ?? "")")
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        // force change
        _ = try? Realm()
        

    }

}
