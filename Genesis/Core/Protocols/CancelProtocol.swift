//
//  CancelProtocol.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import Foundation

protocol CancelProtocol {
    func cancel()
}
