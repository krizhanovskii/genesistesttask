//
//  ErrorParser.swift
//  SWExampleProject
//
//  Created by Krizhanovskii on 3/16/17.
//  Copyright © 2017 skywell.com.ua. All rights reserved.
//

import Foundation

final class ErrorParserHandler : ErrorParserProtocol {

    var statusCode: StatusCode
    var message: String
    
    required init(_ statusCode: StatusCode, message: String) {
        self.statusCode = statusCode
        self.message = message
    }
    
    static func parseError(_ JSON: AnyObject) -> ErrorParserHandler {
        //TODO: error
        guard let message = (JSON as? DictionaryAlias)?["message"] as? String else {
            return ErrorParserHandler(.badRequest, message: "bad request")
        }
        return ErrorParserHandler(.badRequest, message: message)
    }
}
