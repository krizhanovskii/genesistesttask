//
//  ArticleRequest.swift
//  SWExampleProject
//
//  Created by Krizhanovskii on 3/20/17.
//  Copyright © 2017 skywell.com.ua. All rights reserved.
//

import Foundation
import Alamofire

class GHRepositorySearchRequest: APIRequestProtocol {
    internal var parameters: [String: Any]? = nil
    internal var headers: [String: String] = [:]
    typealias Response = GHRepositorySearchResponse
    
    required init(){}
    
    var HTTPMethod: HTTPMethod {return .get}
    
    var path: String {
        return "/search/repositories"
    }
    
}
