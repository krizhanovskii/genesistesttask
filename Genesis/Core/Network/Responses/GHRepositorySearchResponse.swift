//
//  ArticleResponse.swift
//  SWExampleProject
//
//  Created by Krizhanovskii on 3/20/17.
//  Copyright © 2017 skywell.com.ua. All rights reserved.
//

import Foundation

struct GHRepositorySearchResponse: APIResponseProtocol {
    typealias Model = GHRepositoryModel
    var result : Array<Model> = Array<Model>()
    
    init (JSON: DictionaryAlias) {
        if let resp = JSON["items"] as? ArrayOfDictionaries {
            self.result = Model.getList(resp)
        }
    }
}
