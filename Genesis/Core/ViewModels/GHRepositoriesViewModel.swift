import Foundation
import RxSwift
import RxCocoa
import Alamofire

class GHRepositoriesListViewModel: ViewModelProtocol, CancelProtocol {
    
    var result: PublishSubject<[GHRepositoryViewModel]> = PublishSubject<[GHRepositoryViewModel]>()
    
    /// ModelType
    typealias ModelType = GHRepositoryModel
    /// Array or Single object. Example: typealias Model = Array<ModelType> or typealias Model = ModelType
    typealias Model = Array<ModelType>
    /// Status for current request
    var requestStatus: RequestStatus = {
        return .loading
        }() {
        didSet {
            self.viewModelChanged?()
        }
    }
    /// Function for force view model update
    var viewModelChanged: ((Void) -> Void)?
    /// Your model variable
    
    var model: Model {
        didSet {
            self.result.onNext(model.map({GHRepositoryViewModel($0)}))
        }
    }
    
    /// init functions
    required init() {
        self.model = Model()
    }
    required init(_ model: Model) {
        self.model = model
    }
    
    /// Set new Model type
    func set(_ model: Model) {
        self.model = model
    }
    
    //update protocol
    func update() {
        // request data
        self.requestStatus = .loading
    }
    
    fileprivate var disposeBag = DisposeBag()
    
    fileprivate var requests: [Request?] = []
    func update(_ parameters: DictionaryAlias) {
        self.requestStatus = .loading

        let req1 = GHRepositoriesRepository.searchRepositories(parameters, returnRequest: { request in
            self.requests.append(request)
        })
        let req2 = GHRepositoriesRepository.searchRepositories(parameters, returnRequest: { request in
            self.requests.append(request)
        })

        
        var resultArray: Model = []

        Observable<Model>.combineLatest(req1, req2) { (obj1, obj2) -> Model in
            
            let firstArr = obj1.result
            let secondArr = obj2.result
            
            resultArray.append(contentsOf: firstArr[0..<min(15, firstArr.count)])
            
            if secondArr.count > 15 {
                resultArray.append(contentsOf: secondArr[15..<min(30, firstArr.count)])
            }
            
            Log.debug.log("Results \(resultArray.count)")
            return resultArray.sorted(by: { (obj1, obj2) -> Bool in
                return obj1.starCount > obj2.starCount
            })
            
            }.subscribe(onNext: { items in
                
                if let srchReq = parameters["q"] as? String {
                    let ghHistory = GHRepositoryHistoryModel.init()
                    ghHistory.time = Int(UInt64(Date().timeIntervalSince1970))
                    ghHistory.searchRequest = srchReq
                    items.forEach({ghHistory.results.append($0)})
                    
                    RealmResolver.saveModelToStorage(ghHistory)
                }
                
                // delete request
                self.requests.removeAll()
                
                self.set(items)
                self.requestStatus = .success
            }).addDisposableTo(disposeBag)

    }
    
    // MARK: - actions
    func cancel() {
        requests.forEach({$0?.cancel()})
        self.requestStatus = .success
    }
    
    
}

class GHRepositoryViewModel: ViewModelProtocol {
    var id: Int {
        return self.model.id
    }
    
    var name: String {
        return self.model.name
    }
    
    var fullName: String {
        return self.model.fullName
    }
    
    var url: URL? {
        guard self.model.webUrl != stringDummy else {
            return nil
        }
        return URL(string: self.model.webUrl)
    }
    
    var repositoryDescription: String {
        if self.model.repositoryDescription.characters.count > 30 {
            return self.model.repositoryDescription[0..<min(30, self.model.repositoryDescription.characters.count)] + "..."
        }
        return self.model.repositoryDescription
    }
    
    var starCount: Int {
        return self.model.starCount
    }
    
    
    /// ModelType
    typealias ModelType = GHRepositoryModel
    /// Array or Single object. Example: typealias Model = Array<ModelType> or typealias Model = ModelType
    typealias Model = ModelType
    /// Status for current request
    var requestStatus: RequestStatus = {
        return .loading
        }() {
        didSet {
            self.viewModelChanged?()
        }
    }
    /// Function for force view model update
    var viewModelChanged: ((Void) -> Void)?
    /// Your model variable
    
    var model: Model
    
    /// init functions
    required init() {
        self.model = Model()
    }
    required init(_ model: Model) {
        self.model = model
    }
    
    /// Set new Model type
    func set(_ model: Model) {
        self.model = model
    }
}
