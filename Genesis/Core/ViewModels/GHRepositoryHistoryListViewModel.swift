import Foundation
import RxSwift
import RxCocoa
import Alamofire

class GHRepositoryHistoryListViewModel: ViewModelProtocol {
    
    
    var result: PublishSubject<[GHRepositoryHistoryViewModel]> = PublishSubject<[GHRepositoryHistoryViewModel]>()
    
    /// ModelType
    typealias ModelType = GHRepositoryHistoryModel
    /// Array or Single object. Example: typealias Model = Array<ModelType> or typealias Model = ModelType
    typealias Model = Array<ModelType>
    /// Status for current request
    var requestStatus: RequestStatus = {
        return .loading
        }() {
        didSet {
            self.viewModelChanged?()
        }
    }
    /// Function for force view model update
    var viewModelChanged: ((Void) -> Void)?
    /// Your model variable
    
    var model: Model {
        didSet {
            self.result.onNext(model.map({GHRepositoryHistoryViewModel($0)}))
        }
    }
    
    /// init functions
    required init() {
        self.model = Model()
    }
    required init(_ model: Model) {
        self.model = model
    }
    
    /// Set new Model type
    func set(_ model: Model) {
        self.model = model
    }
    
    //update protocol
    let disposeBag = DisposeBag()
    func update() {
        // request data
        self.requestStatus = .loading
        GHRepositoriesRepository.fetchHistory(GHRepositoryHistoryModel.self)
            .subscribe(onNext: { items in
                self.set(items)
                self.requestStatus = .success
            }, onError: { error in
                self.requestStatus = .error(ErrorParserHandler.handleError(error))
            }).addDisposableTo(disposeBag)
    }
}

class GHRepositoryHistoryViewModel: ViewModelProtocol {
    var time: Int {
        return self.model.time
    }
    
    var searchRequest: String {
        return self.model.searchRequest
    }
    
    var objects: Array<GHRepositoryViewModel> {
        return self.model.results.map({GHRepositoryViewModel($0)})
    }
    
    
    /// ModelType
    typealias ModelType = GHRepositoryHistoryModel
    /// Array or Single object. Example: typealias Model = Array<ModelType> or typealias Model = ModelType
    typealias Model = ModelType
    /// Status for current request
    var requestStatus: RequestStatus = {
        return .loading
        }() {
        didSet {
            self.viewModelChanged?()
        }
    }
    /// Function for force view model update
    var viewModelChanged: ((Void) -> Void)?
    /// Your model variable
    
    var model: Model
    
    /// init functions
    required init() {
        self.model = Model()
    }
    required init(_ model: Model) {
        self.model = model
    }
    
    /// Set new Model type
    func set(_ model: Model) {
        self.model = model
    }
}
