//
//  GHRepositoryWebShowerView.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit

class GHRepositoryWebShowerView: UIView, RegisterViewProtocol {

    static var shared = GHRepositoryWebShowerView()
    
    // MARK: - outlets
    @IBOutlet fileprivate var webView: UIWebView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!

    var isShowing : Bool {
        return self.alpha == 0 ? false : true
    }
    
    // MARK: - RegisterViewProtocol
    var view: UIView! {
        didSet {
            configure()
        }
    }
    
    func configure() {
        self.alpha = 0
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        self.webView.layer.cornerRadius = 6
        self.webView.layer.masksToBounds = true
        self.webView.delegate = self
        
        self.activityIndicator.hidesWhenStopped = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        ({ view = xibSetuView() })()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        ({ view = xibSetuView() })()
    }
    
    convenience init () {
        self.init(frame: UIScreen.main.bounds)
    }
    
    
    // Close when touch outside
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let location = touches.first?.location(in: self) {
            if !webView.frame.contains(location) {
                self.close()
            }
        }
    }
    
    //MARK: show
    public func show(_ webURL: URL) {
        self.webView.loadRequest(URLRequest(url: webURL))
        
        if self.isShowing {
            return
        }
        
        if UIApplication.topViewController()?.navigationController != nil {
            self.alpha = 0
            UIApplication.topViewController()?.navigationController?.view.addSubview(self)
        } else {
            self.alpha = 0
            UIApplication.topViewController()?.view.addSubview(self)
        }
        self.fadeIn()
    }
    
    public func close() {
        if !self.isShowing {
            return
        }
        self.fadeOut()
    }
    
    //animation
    /* animation show \ close */
    fileprivate func fadeIn() {
        UIView.animate(withDuration: 0.7, animations: { () -> Void in
            self.alpha = 1
        }, completion: { (complection) -> Void in
        })
    }
    
    fileprivate func fadeOut() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0
        }, completion: { (complection) -> Void in
            self.removeFromSuperview()
        })
    }
}

extension GHRepositoryWebShowerView: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
}
