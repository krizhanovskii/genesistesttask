//
//  LoaderView.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit

protocol CancelDelegate: class {
    func cancel()
}

class LoaderView: UIView, RegisterViewProtocol {
    
    // MARK: - outlets
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var btnCancel: UIButton!

    weak var delegate: CancelDelegate?
    
    
    var animate: Bool = false {
        didSet {
            animate ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
    }
    
    // MARK: - RegisterViewProtocol
    var view: UIView! {
        didSet {
            configure()
        }
    }
    
    func configure() {
        self.view.backgroundColor = .green
        self.activityIndicator.hidesWhenStopped = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        ({ view = xibSetuView() })()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        ({ view = xibSetuView() })()
    }
    
    @IBAction fileprivate func btnCancelPressed(_ button: UIButton) {
        self.delegate?.cancel()
    }
}
