//
//  GHRepositorySearchHistoryContentView.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GHRepositorySearchHistoryContentView: UIView, RegisterViewProtocol, DataProviderProtocol {
    
    fileprivate var disposebag = DisposeBag()
    // MARK: - outlets
    @IBOutlet fileprivate var tableView: UITableView!
    typealias DataType = Array<GHRepositoryHistoryViewModel>
    
    fileprivate var tableData: PublishSubject<DataType> = PublishSubject<DataType>()
    
    fileprivate typealias Cell = GHRepositoryHistoryCell
    
    // MARK: - RegisterViewProtocol
    var view: UIView! {
        didSet {
            configure()
        }
    }
    
    func configure() {
        self.tableView.registerNib(Cell.self)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        self.configureRX()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        ({ view = xibSetuView() })()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        ({ view = xibSetuView() })()
    }
    
    fileprivate func configureRX() {
        self.tableData
            .asObserver()
            .bindTo(self.tableView.rx.items(cellIdentifier: Cell.nibName, cellType: Cell.self)) { row, data, cell in
                cell.set(data: data)
            }
            .addDisposableTo(disposebag)
        
        self.tableView.rx.modelSelected(GHRepositoryHistoryViewModel.self).asObservable().subscribe(onNext: {item in
            Log.info.log("Item selected: \(item.searchRequest)")
            if let vc = self.parentViewController?.storyboard?.instantiateViewController(GHRepositoryHistoryResultCntrl.self) {
                self.parentViewController?.navigationController?.pushViewController(viewController: vc, animated: true, completion: { 
                    vc.set(data: item.objects)
                })
            }
        }).addDisposableTo(disposebag)
    }
    
    func set(data: DataType) {
        self.tableData.onNext(data)
    }
}
