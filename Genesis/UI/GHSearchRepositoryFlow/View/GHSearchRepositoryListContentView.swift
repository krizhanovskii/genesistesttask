//
//  GHSearchRepositoryListContentView.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GHSearchRepositoryListContentView: UIView, RegisterViewProtocol, DataProviderProtocol {
    fileprivate var disposebag = DisposeBag()
    // MARK: - outlets
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var viewLoader: LoaderView!
    @IBOutlet fileprivate var contraintToTop: NSLayoutConstraint!

    typealias DataType = Array<GHRepositoryViewModel>
    
    fileprivate var tableData: PublishSubject<DataType> = PublishSubject<DataType>()

    // MARK: - RegisterViewProtocol
    var view: UIView! {
        didSet {
            configure()
        }
    }
    
    func configure() {
        self.tableView.registerNib(GHRepositoryCell.self)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        
        self.viewLoader.delegate = self
        self.setLoaderShowed(false, animating: false)
        
        self.configureRX()
    }
    
    func setLoaderShowed(_ flag: Bool, animating: Bool = true) {
        self.viewLoader.animate = flag
        UIView.animate(withDuration: animating ? 0.7 : 0.0) {
            if flag {
                self.contraintToTop.constant = 0
            } else {
                self.contraintToTop.constant = 0 - self.viewLoader.frame.height
            }
            self.view.layoutIfNeeded()
        }
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        ({ view = xibSetuView() })()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        ({ view = xibSetuView() })()
    }
    
    fileprivate func configureRX() {
        self.tableData
            .asObserver()
            .bindTo(self.tableView.rx.items(cellIdentifier: GHRepositoryCell.nibName, cellType: GHRepositoryCell.self)) { row, data, cell in
                cell.set(data: data)
            }
            .addDisposableTo(disposebag)
        
        self.tableView.rx.modelSelected(GHRepositoryViewModel.self).asObservable().subscribe(onNext: {item in
            Log.info.log("Item selected")
            guard let url = item.url else {
                return
            }
            GHRepositoryWebShowerView.shared.show(url)
            
        }).addDisposableTo(disposebag)
    }
    
    func set(data: DataType) {
        self.tableData.onNext(data)
    }
}

extension GHSearchRepositoryListContentView: CancelDelegate {
    func cancel() {
        (self.parentViewController as? CancelProtocol)?.cancel()
    }
}
