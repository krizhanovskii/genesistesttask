//
//  GHRepositoryCell.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit

class GHRepositoryCell: UITableViewCell, RegisterCellProtocol, DataProviderProtocol {
    
    typealias DataType = GHRepositoryViewModel

    // MARK: - outlets
    @IBOutlet fileprivate var lblName: UILabel!
    @IBOutlet fileprivate var lblDescription: UILabel!
    @IBOutlet fileprivate var lblStarsCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblName.textColor = .black
        lblDescription.textColor = .gray
        lblStarsCount.textColor = .black
    }
    
    func set(data: DataType) {
        lblName.text = data.fullName
        lblDescription.text = data.repositoryDescription
        lblStarsCount.text = "Stars \(data.starCount)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
}
