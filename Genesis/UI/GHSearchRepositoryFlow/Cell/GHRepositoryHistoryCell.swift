//
//  GHRepositoryHistoryCell.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit

class GHRepositoryHistoryCell: UITableViewCell, RegisterCellProtocol, DataProviderProtocol {
    
    typealias DataType = GHRepositoryHistoryViewModel
    
    // MARK: - outlets
    @IBOutlet fileprivate var lblSearchName: UILabel!
    @IBOutlet fileprivate var lblRepositoriesCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblSearchName.textColor = .black
        lblRepositoriesCount.textColor = .gray
    }
    
    func set(data: DataType) {
        lblSearchName.text = data.searchRequest
        lblRepositoriesCount.text = "Repos count: \(data.objects.count)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
}
