//
//  GHRepositorySearchHistoryCntrl.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class GHRepositorySearchHistoryCntrl: UIViewController {

    fileprivate var disposeBag = DisposeBag()
    
    // MARK: - outlets
    @IBOutlet fileprivate var viewContent: GHRepositorySearchHistoryContentView!
    
    // viewModel
    var viewModel: GHRepositoryHistoryListViewModel! {
        didSet {
            self.viewModel!.viewModelChanged = { Void in
                switch self.viewModel!.requestStatus {
                case .loading:
                    Log.info.log("Queue1 loading")
                    break;
                case .error(let err):
                    Log.info.log("Queue1 error \(err.message)")
                    break;
                case .success:
                    Log.info.log("Queue1 success")
                }
            }
        }
    }
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = true
        self.title = "History"


        self.viewModel = GHRepositoryHistoryListViewModel()
        self.configureRX()

        self.viewModel.update()
    }
    
    // MARK: - rx
    fileprivate func configureRX() {
        self.viewModel.result.asObserver().subscribe(onNext: { items in
            Log.debug.log("in controller \(items.count)")
            self.viewContent.set(data: items.sorted(by: { (obj1, obj2) -> Bool in
                return obj1.time > obj2.time
            }))
        }).addDisposableTo(disposeBag)
    }
}

