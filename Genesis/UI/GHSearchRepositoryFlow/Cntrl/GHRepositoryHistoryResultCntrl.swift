//
//  GHRepositoryHistoryResultCntrl.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class GHRepositoryHistoryResultCntrl: UIViewController, DataProviderProtocol {
    typealias DataType = Array<GHRepositoryViewModel>

    @IBOutlet fileprivate var viewContent: GHSearchRepositoryListContentView!

    func set(data: DataType) {
        self.viewContent.set(data: data)
    }
}
