//
//  GHSearchRepositoryListCntrl.swift
//  Genesis
//
//  Created by Krizhanovskii on 5/26/17.
//  Copyright © 2017 k.krizhanovskii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GHSearchRepositoryListCntrl: UIViewController {
    
    fileprivate var disposeBag = DisposeBag()
    
    // MARK: - outlets
    @IBOutlet fileprivate var viewContent: GHSearchRepositoryListContentView!
    fileprivate lazy var searchBar: UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 32 - 60, height: 30))

    // viewModel
    var viewModel: GHRepositoriesListViewModel! {
        didSet {
            self.viewModel!.viewModelChanged = { Void in
                switch self.viewModel!.requestStatus {
                case .loading:
                    Log.info.log("loading")
                    self.viewContent.setLoaderShowed(true)
                    break;
                case .error(let err):
                    Log.info.log("error \(err.message)")
                    self.viewContent.setLoaderShowed(false)
                    break;
                case .success:
                    Log.info.log("success")
                    self.viewContent.setLoaderShowed(false)

                }
            }
        }
    }
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
                
        searchBar.placeholder = "Search"
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        self.navigationController?.setNavigationBarStyle(.default)
        
        let rightNavBarButton = UIBarButtonItem(title: "History", style: .done, target: self, action: #selector(self.openHistory))
        self.navigationItem.rightBarButtonItem = rightNavBarButton
        
        self.viewModel = GHRepositoriesListViewModel()
        self.configureRX()
    }
    
    func openHistory() {
        if let vc = self.storyboard?.instantiateViewController(GHRepositorySearchHistoryCntrl.self) {
            self.show(vc, sender: nil)
        }
    }
    
    // MARK: - rx
    fileprivate func configureRX() {
        self.searchBar.rx.text.orEmpty
            .debounce(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { text in
                if !text.isEmpty {
                   self.viewModel.update(["q":text])
                }
            })
            .addDisposableTo(disposeBag)
        
        self.viewModel.result.asObserver().subscribe(onNext: { items in
            Log.debug.log("in controller \(items.count)")
            self.viewContent.set(data: items)
        }).addDisposableTo(disposeBag)
    }
}

extension GHSearchRepositoryListCntrl: StoryboardProtocol {
    static var storyboardName: String {
        return "GHSearchRepositoryFlow"
    }
}

extension GHSearchRepositoryListCntrl: CancelProtocol {
    func cancel() {
        self.viewModel.cancel()
    }
}
